import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.scss']
})
export class UiComponent {

  constructor(@Inject(DOCUMENT) private document: Document
  ) { }

  toggleDarkMode(): void {
    this.document.body.classList.toggle('u-theme-light');
  }

}
